package avm;

import avr.base.Registry;
import avr.util.Delay;

/**
 * LED blink test
 * 
 * @author shjeff
 *
 */
public class Test2
{
	public static void main(String[] args)
	{
		// D5, D6, D7
		
		// 0x11 DDRD
		// 0x12 PORTD
		// 0x17 DDRB
		// 0x18 PORTB
		
		Registry.setBit(0x11, 5);
		Registry.setBit(0x11, 6);
		Registry.setBit(0x11, 7);
		
		Registry.clearBit(0x12, 5);
		Registry.clearBit(0x12, 6);
		Registry.clearBit(0x12, 7);
		
		for(;;) {
			// D5 = ON
			Registry.setBit(0x12, 5);
			Registry.clearBit(0x12, 6);
			Registry.clearBit(0x12, 7);
			Delay.msec(250);
			
			// D6 = ON
			Registry.clearBit(0x12, 5);
			Registry.setBit(0x12, 6);
			Registry.clearBit(0x12, 7);
			Delay.msec(250);
			
			// D7 = ON
			Registry.clearBit(0x12, 5);
			Registry.clearBit(0x12, 6);
			Registry.setBit(0x12, 7);
			Delay.msec(250);
			
			// D6 = ON
			Registry.clearBit(0x12, 5);
			Registry.setBit(0x12, 6);
			Registry.clearBit(0x12, 7);
			Delay.msec(250);
		}
	}
}
