package avm;

import avr.base.Registry;
import avr.util.Delay;

/**
 * LED blink test
 * 
 * @author shjeff
 *
 */
public class Test2
{
	public static void main(String[] args)
	{
		// 0x17 DDRB
		// 0x18 PORTB
		
		Registry.setBit(0x17, 0);
		Registry.setBit(0x17, 1);
		
		Registry.setBit(0x18, 1);
		Registry.clearBit(0x18, 0);
		
		for(;;) {
			Registry.toggleBit(0x18, 0);
			Delay.msec(250);
		}
	}
}
