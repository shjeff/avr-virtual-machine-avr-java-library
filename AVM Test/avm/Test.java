package avm;

import avr.base.OutputPin;
import avr.base.chip.AtMega8;
import avr.lib.display.lcd.alphanumeric.HD44780;

public class Test
{
	HD44780 lcd1 = new HD44780(16, 2);
	HD44780 lcd2 = new HD44780(16, 2);
	HD44780 lcd3 = new HD44780(16, 2);
	
	public static void main(String[] args)
	{
		new Test();
	}
	
	public Test()
	{
		lcd1.config4Line(
				new OutputPin(AtMega8.PortC, 0), new OutputPin(AtMega8.PortC, 1), 
				new OutputPin(AtMega8.PortC, 2), new OutputPin(AtMega8.PortC, 3));
		
		lcd1.configBusyFlagRead(false);
		lcd1.configControlLines(new OutputPin(AtMega8.PortC, 4), new OutputPin(AtMega8.PortC, 5));
		
		
		lcd2.config4Line(
				new OutputPin(AtMega8.PortB, 5), new OutputPin(AtMega8.PortB, 4), 
				new OutputPin(AtMega8.PortB, 3), new OutputPin(AtMega8.PortB, 2));
		
		lcd2.configBusyFlagRead(false);
		lcd2.configControlLines(new OutputPin(AtMega8.PortB, 1), new OutputPin(AtMega8.PortB, 0));
		
		
		lcd3.config4Line(
				new OutputPin(AtMega8.PortD, 0), new OutputPin(AtMega8.PortD, 1), 
				new OutputPin(AtMega8.PortD, 2), new OutputPin(AtMega8.PortD, 3));
		
		lcd3.configBusyFlagRead(false);
		lcd3.configControlLines(new OutputPin(AtMega8.PortD, 4), new OutputPin(AtMega8.PortD, 5));
		
		
		lcd1.init();
		lcd2.init();
		lcd3.init();
		
		lcd1.writeText("Hello ATH");
		lcd2.writeText("Hello Java");
		lcd3.writeText("Hello AVR");
	}
}
