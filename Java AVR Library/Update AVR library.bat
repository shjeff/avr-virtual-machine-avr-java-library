@ECHO OFF
ECHO Compiling all Java AVR library
SET CURRENT_ROOT_DIRECTORY=.
CALL :LCompileJavaFilesInDirectories "%CURRENT_ROOT_DIRECTORY%"
PAUSE
EXIT 0

:LCompileJavaFilesInDirectory
ECHO Compiling files in location:
ECHO %~1
CD %~1
IF EXIST ./*.java (
JAVAC ./*.java
SET JAVAC_EXIT_CODE=%ERRORLEVEL%
ECHO Exit status: %JAVAC_EXIT_CODE%
IF "%JAVAC_EXIT_CODE%" EQU "" (
ECHO Compilation successful
) ELSE (
ECHO An error occured while compiling library
PAUSE
EXIT 1
)


) ELSE (
ECHO No any Java files found
)
GOTO:EOF

:LCompileJavaFilesInDirectories
FOR /D /r %%I in (*) DO (
CALL :LCompileJavaFilesInDirectory "%%I"
)