package avr.base;

public class OutputPin
{
	PortSet _port;
	int _pin = 0;
	
	public OutputPin(PortSet port, int pinNumber)
	{
		Registry.setBit(port.RDDR, pinNumber);
		_port = port;
		_pin = pinNumber;
	}
	
	public void setBit()
	{
		Registry.setBit(_port.RPORT, _pin);
	}
	
	public void clearBit()
	{
		Registry.clearBit(_port.RPORT, _pin);
	}
}
