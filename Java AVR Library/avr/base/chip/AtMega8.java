package avr.base.chip;

import avr.base.PortSet;

public class AtMega8
{
	public static int RPIND = 0x10;
	public static int RDDRD = 0x11;
	public static int RPORTD = 0x12;
	
	public static int RPINC = 0x13;
	public static int RDDRC = 0x14;
	public static int RPORTC = 0x15;
	
	public static int RPINB = 0x16;
	public static int RDDRB = 0x17;
	public static int RPORTB = 0x18;
	
	public static PortSet PortB = new PortSet(RDDRB, RPINB, RPORTB);
	
	public static PortSet PortC = new PortSet(RDDRC, RPINC, RPORTC);
	
	public static PortSet PortD = new PortSet(RDDRD, RPIND, RPORTD);
	
	public static class ISRVectors
	{
		public static int RST = 0x00;
		public static int INT0 = 0x01;
		public static int INT1 = 0x02;
		public static int TMR2CMP = 0x03;
		public static int TMR2OVF = 0x04;
		
		public static int TMR1CPT = 0x05;
		public static int TMR1CMPA = 0x06;
		public static int TMR1CMPB = 0x07;
		public static int TMR1OVF = 0x08;
		
		public static int TMR0OVF = 0x09;
		public static int SPISTC = 0x0A;
		public static int USARTRXC = 0x0B;
		public static int USARTDRE = 0x0C;
		public static int USARTTXC = 0x0D;
		public static int ADC = 0x0E;
		public static int EERD = 0x0F;
		public static int ANCMP = 0x10;
		public static int TWI = 0x11;
		public static int SPMRD = 0x12;
	}
}
