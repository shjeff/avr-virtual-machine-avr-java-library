package avr.base;

public class PortSet
{
	public int RDDR, RPIN, RPORT;
	
	public PortSet(int registryDDRx, int registryPINx, int registryPORTx)
	{
		RDDR = registryDDRx;
		RPIN = registryPINx;
		RPORT = registryPORTx;
	}
}
