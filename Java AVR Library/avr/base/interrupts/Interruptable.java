package avr.base.interrupts;

public interface Interruptable
{
	void interrupt(int interruptId);
}
