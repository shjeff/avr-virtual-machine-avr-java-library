package avr.base.interrupts;

public class Interrupts
{
	/**
	 * Registers specified interrupt
	 * @param instance				interrupt class (implements Interruptable)
	 * @param interruptId			interrupt id
	 */
	public static native void register(Interruptable instance, int interruptId);
	
	public static native void enable();
	
	public static native void disable();
}
