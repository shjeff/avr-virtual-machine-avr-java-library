package avr.base;

public class InputPin
{
	PortSet _port;
	int _pin = 0;
	
	public InputPin(PortSet port, int pinNumber, int pullUpState)
	{
		Registry.clearBit(port.RDDR, pinNumber);
		Registry.setBitState(port.RPORT, pinNumber, pullUpState);
		_port = port;
		_pin = pinNumber;
	}
	
	public int getBit()
	{
		return Registry.getBit(_port.RPIN, _pin);
	}
}
