package avr.base;

public class Registry
{
	/**
	 * Sets 8-bit value in specific register
	 * @param address				register address
	 * @param value					register new value
	 */
	public static native void set(int address, int value);
	
	/**
	 * Gets 8-bit value from specific register
	 * @param address				register address
	 * @return						register value
	 */
	public static native int get(int address);
	
	/**
	 * Sets specified bit in register
	 * @param address				register address
	 * @param bit					bit number
	 */
	public static native void setBit(int address, int bit);
	
	/**
	 * Clears specified bit in register
	 * @param address				register address
	 * @param bit					bit number
	 */
	public static native void clearBit(int address, int bit);
	
	/**
	 * Sets bit state in register
	 * @param address				register address
	 * @param bit					bit number
	 * @param state					state to set
	 */
	public static native void setBitState(int address, int bit, int state);
	
	/**
	 * Toggles specified bit in register
	 * @param address				register address
	 * @param bit					bit number
	 */
	public static native void toggleBit(int address, int bit);
	
	/**
	 * Gets specified bit state from register
	 * @param address				register address
	 * @param bit					bit number
	 * @return						bit state
	 */
	public static native int getBit(int address, int bit);
}
