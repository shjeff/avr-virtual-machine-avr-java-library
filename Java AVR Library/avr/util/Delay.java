package avr.util;

public class Delay
{
	public static native void sec(int seconds);
	
	public static native void msec(int miliseconds);
	
	public static native void usec(int microseconds);
}
