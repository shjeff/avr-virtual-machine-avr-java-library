package avr.lib.display.lcd.alphanumeric;

import avr.base.InputPin;
import avr.base.OutputPin;

public class HD44780
{
	OutputPin _db0, _db1, _db2, _db3,
		_db4, _db5, _db6, _db7;
	
	OutputPin _rs, _e;
	InputPin _rw;
	
	int _displayMode = 0;
	
	boolean _busyFlagRead = false;
	
	int _width = 0, _height = 0;
	
	public HD44780(int width, int height)
	{
		_width = width;
		_height = height;
	}
	
	public void init()
	{
		
	}
	
	public void config8Line(OutputPin db0, OutputPin db1, OutputPin db2, OutputPin db3, OutputPin db4, OutputPin db5, OutputPin db6, OutputPin db7)
	{
		_db0 = db0;
		_db1 = db1;
		_db2 = db2;
		_db3 = db3;
		_db4 = db4;
		_db5 = db5;
		_db6 = db6;
		_db7 = db7;
	}
	
	public void config4Line(OutputPin db4, OutputPin db5, OutputPin db6, OutputPin db7)
	{
		_db4 = db4;
		_db5 = db5;
		_db6 = db6;
		_db7 = db7;
	}
	
	public void configBusyFlagRead(boolean useFlagReading)
	{
		_busyFlagRead = useFlagReading;
	}
	
	public void configControlLines(OutputPin rs, OutputPin e)
	{
		_rs = rs;
		_e = e;
	}
	
	public void configRWPin(InputPin rw)
	{
		_rw = rw;
	}
	
	public void writeText(String text)
	{
		
	}
}
